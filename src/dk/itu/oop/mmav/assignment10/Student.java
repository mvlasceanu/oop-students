/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */
package dk.itu.oop.mmav.assignment10;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Student implements Serializable, Cloneable {
    
    // Instance counter
    public static int COUNT_ST = 0;
    // Student name
    protected String name;
    // Student group
    protected String group;
    // Student proficiency in Java
    protected double proficiencyInJava;
    // Student CPR
    protected String cprNumber;
    // Student Gender
    protected String gender;
    // Student Grades
    protected final ArrayList<Grade> studentGrades = new ArrayList<>();
    // A counter for the total number of courses/grades
    protected int total_grades;
    // GPA
    protected float gpa;
    // Addition for Assignment 4
    protected String degree_level;
    // file to store sudent collection
    protected String file = "Students.dat";
    
    /**
     * Stores the student's course support (TA & PhD)
     * 
     * @packasge Assignment 4, exercise 2&3
     */
    protected ArrayList courseSupport = new ArrayList<>();
    
    /**
     * Constructor
     * 
     * @param newName
     * @param newProficiencyInJava
     * @param newCprNumber
     * @param newGender 
     */
    public Student(String newName, double newProficiencyInJava, String newCprNumber, String newGender) {
        this.setName(newName);
        this.setGroup(Constants.DEFAULT_ST_GROUP);
        this.setProficiencyInJava(newProficiencyInJava);
        this.setCprNumber(newCprNumber);
        this.setGender(newGender);
        this.setStudentGrades();
        this.getInstanceMessage();
        MyLog.log("A new " + this.getClass().getName() + " instance has been created: " + this.toString(), MyLog.TO_FILE);
    }

    /**
     * No params constructor
     */
    public Student() {
    }
    
    /**
     * COnstructor with name
     * @param name 
     */
    public Student(String name) {
        this.name = name;
    }
    
    /**
     * Override toString()
     * @return 
     */
    @Override public String toString() {
        StringBuilder record = new StringBuilder();
        record.append(" " + Constants.NAME + ": ").append(this.getName()).append(Constants.NEW_LINE);
        record.append(" " + Constants.GROUP + ": ").append(this.getGroup()).append(Constants.NEW_LINE);
        record.append(" " + Constants.JAVA_PROF + ": ").append(this.getProficiencyInJava()).append(Constants.NEW_LINE);
        record.append(" " + Constants.CPR + ": ").append(this.getCprNumber()).append(Constants.NEW_LINE);
        record.append(" " + Constants.GENDER + ": ").append(this.getGender()).append(Constants.NEW_LINE);
        if(!"".equals(this.getDegreeLevel()))
        {
            record.append(" " + Constants.ADDSTUD_DL_LBL + ": ").append(this.getDegreeLevel()).append(Constants.NEW_LINE);
        }
        return record.toString();
    }
    
    /**
     * 
     * Override equals()
     * 
     * @param obj
     * @return 
     */
    @Override public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if (!(obj instanceof Student))
            return false;
        Student rhs = (Student) obj;
        return (
                this.getGender().equals(rhs.getGender())
                && this.getCprNumber().equals(rhs.getCprNumber()) 
                && this.getProficiencyInJava()== rhs.getProficiencyInJava()
                && this.getGroup().equals(rhs.getGroup())
                && this.getName().equals(rhs.getName())
                && this.getDegreeLevel().equals(rhs.getDegreeLevel())
                && this.getCourseSupport().equals(rhs.getCourseSupport()));
    }
    
    /**
     * Override clone()
     * 
     * @return
     * @throws CloneNotSupportedException 
     */
    @Override protected Student clone() throws CloneNotSupportedException {
        return (Student) super.clone();
    }
    
    /**
     * Returns student's name
     * 
     * @return 
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Sets the student's name
     * 
     * @package Assignment 4 -> Changed to 'final'
     * @param name 
     */
    public final void setName(String name)
    {
        // Returns desired result
        this.name = name;
        
        // Returns null because we're trying to assign an undefined value
        // to method's parameter (the opposite of the purpose)
        //name = this.name;
        
        // returns null because we're trying to assign a value to a locsl variable
        // not to the object
        //name = name;
    }
   
    /**
     * Returns student's group
     * 
     * @return 
     */
    public String getGroup()
    {
        return this.group;
    }
    
    /**
     * Set student group
     * 
     * @param newGroup 
     */
    public void setGroup(String newGroup)
    {
        this.group = newGroup;
    }
    
    /**
     * Gets current student's Java proficiency
     * 
     * @return 
     */
    public double getProficiencyInJava() {
        return proficiencyInJava;
    }
    
    /**
     * It set's current student's Java Proficency
     * 
     * @package Assignment 4: Changed to  'final'
     * @param newProficiency 
     */
    public final void setProficiencyInJava(double newProficiency) {
        this.proficiencyInJava = newProficiency;
    }
    
    /**
     * Get CPR
     * 
     * @return 
     */
    public String getCprNumber() {
        return cprNumber;
    }
    
    /**
     * Sets student's cpr
     * 
     * @package Assignment 4 -> Changed to 'final'
     * @param newCpr 
     */
    public final void setCprNumber(String newCpr) {
        this.cprNumber = newCpr;
    }
    
    /**
     * Get gender
     * 
     * @return 
     */
    public String getGender() {
        return this.gender;
    }
    
    /**
     * Set's the gender of the student
     * 
     * @package Assignment 4 -> Changed to 'final'
     * @param newGender 
     */
    public final void setGender(String newGender)
    {
        this.gender = newGender;
    }
    
    /**
     * Compares if the current instance has the same
     * Java proficiency with another passed instance
     * 
     * @param anotherStudent
     * @return 
     */
    public boolean hasSameFluencyInJavaAs(Student anotherStudent)
    {
        return (this.proficiencyInJava == anotherStudent.proficiencyInJava);
    }
    
    /**
     * Method that randomly gives a student some grades
     * 
     * @package Assignment 4: Changed to final
     */
    public final void setStudentGrades()
    {
        Random ran          = new Random();
        int ttl_courses     = ran.nextInt(6) + 1;
        this.total_grades   = ttl_courses;
        String[] courses    = Student.getPossibleCourses();
        float grade_sum     = 0;
        int Low             = 0;
        int High            = courses.length - 1;
        for(int i = 0; i < ttl_courses; i++)
        {
            float grade     = (float) Math.random()*12;

            int c_index     = ran.nextInt(High-Low) + Low;
            String course   = courses[c_index];

            while(this.studentGrades.contains(course))
            {
                c_index     = ran.nextInt(High-Low) + Low;
                course      = courses[c_index];
            }
            grade_sum       = grade_sum + grade;
            this.gpa        = grade_sum / this.total_grades;
            try {
                Grade g         = new Grade(course, grade);
                this.studentGrades.add(g);
            } catch( Exception e)
            {
                Cli.m(e.getMessage());
            }
            
        }
    }
    
    /**
     * A static list of course names
     * 
     * @return 
     */
    public static String[] getPossibleCourses()
    {
        String[] c = {"Object-Oriented Programming",
                      "Databases",
                      "Mathemathics",
                      "Databases Tuning",
                      "Chemistry",
                      "Physics",
                      "Mobile App Development",
                      "Operating Systems",
                      "Geography",
                      "Model Driven Development Project", 
                      "Programming Workshop",
                      "Introduction to Database Design",
                      "Database Tuning I"};
            return c;
    }
    
    /**
     * Student grades
     * 
     * @return 
     */
    public ArrayList getStudentGrades()
    {
        return this.studentGrades;
    }
    
    /**
     * It converts grades o a string
     * 
     * @return 
     */
    public String getStudentGradesString()
    {
        StringBuilder record = new StringBuilder();
        for (Grade grade : this.studentGrades) {
            record.append(" ").append(grade.getCourse()).append(": ").append(grade.getValue()).append(Constants.NEW_LINE);
        }
        record.append(" " + Constants.TOTAL_COURSES + ": ").append(this.total_grades).append(Constants.NEW_LINE);
        record.append(" " + Constants.GPA + " : ").append(this.averageNote()).append(Constants.NEW_LINE);
        return record.toString();
    }
    
    /**
     * Calculates average grade based on grades
     * 
     * @return 
     */
    public float averageNote()
    {
        return this.gpa;
    }
    
    /**
     * Displays a message in the console regarding the
     * number of instances of this class
     */
    public void getInstanceMessage()
    {
        COUNT_ST++;
        MyLog.log(Constants.DEF_NEW_STD_MSG.replace("{{:x:}}", "" + Student.COUNT_ST + ""), MyLog.TO_CLI);
    }
    
    /**
     * A static list of students
     * 
     * @return 
     */
    public static ArrayList getStaticStudentsList()
    {
        ArrayList<Student> Students = new ArrayList<>();
        // Fixed student objects
        Students.add(new Student("John Doe",            12, "290190-1871", "M"));
        Students.add(new Student("John Doe",            7.5, "290190-1871", "M"));
        Students.add(new Student("Hugh Laurie",         12,   "110659-2564", "M"));
        Students.add(new Student("Jennifer Morrison",   7.5, "120479-8547", "F"));
        Students.add(new Student("Lisa Edelstein",      10,  "210566-6532", "F"));
       
       return Students;
    }
    
    /**
     * Returns the student's degree level
     * 
     * @packasge Assignment 4, exercise 2&3
     * @return 
     */
    public String getDegreeLevel()
    {
        return this.degree_level;
    }
    
    /**
     * Set the current student's degree level
     * 
     * @packasge Assignment 4, exercise 2&3
     * @param level 
     */
    public void setDegreeLevel(String level)
    {
        this.degree_level = level;
    }
    
    /**
     * Get the current student's degree level
     * 
     * @packasge Assignment 4, exercise 2&3
     * @return 
     */
    public static String[] getDegreeLevels()
    {
        String[] dgree_levels = {Constants.DEGREE_BSC, Constants.DEGREE_MSC, Constants.DEGREE_PHD};
        return dgree_levels;
    }
    
    /**
     * Checks if the current students has support for any courses
     * 
     * @packasge Assignment 4, exercise 2&3
     * @return 
     */
    public boolean isTa()
    {
        return (this.courseSupport.size() > 0);
    }
    
        
    /**
     * Gets the student's course support
     * 
     * @packasge Assignment 4, exercise 2&3
     * @return ArrayList
     */
    public ArrayList getCourseSupport()
    {
        return this.courseSupport;
    }
    
    /**
     * Adds an item to supported courses (TA or PhD)
     * 
     * @packasge Assignment 4, exercise 2&3
     * @param items 
     */
    public void addCourseSupport(List items)
    {
       for(Object item : items)
       {
           System.out.println(item);
           this.courseSupport.add(item);
       }
    }
    
    /**
     * A string of supported courses
     * 
     * @return 
     */
    public StringBuilder supportCoursesToString()
    {
        StringBuilder output = new StringBuilder("");
        for(Object item : this.courseSupport)
        {
            output.append(item.toString()).append(Constants.NEW_LINE);
        }
        return output;
    }
    
    /**
     * Checks if the typed string is valid
     * for gender (M or F)
     * 
     * @package Assignment 5, Exercise 5
     * @param g
     * @return 
     */
    public static boolean isValidGender(String g)
    {
        return (g.startsWith("M") || g.startsWith("F"));
    }
    
    /**
     * Assignment 4, Exercise 5
     */
    void display() { System.out.println("I am a regular student"); }
    
    /**
     * Save object to file
     * @package Assignment 10, Exercise 2
     */
    public void saveToFile()
    {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(this.file))) {
            out.writeObject(this);
            out.close();
            MyLog.log("The following instance of " + this.getClass().getName() + " was written to " + this.file + ": " + this.toString(), MyLog.TO_FILE);
        } catch(IOException e)
        {
            Cli.m(e.getMessage());
        }
    }
    
    /**
     * Reads object from file
     * 
     * @package Assignment 10, Exercise 2
     * @return 
     */
    public Student readFromFile()
    {
        try {
            FileInputStream studentFile = new FileInputStream(this.file);
            BufferedInputStream bis = new BufferedInputStream(studentFile);
            
            ObjectInputStream reader = new ObjectInputStream(bis);
            Student stud = (Student) reader.readObject();
        return stud;
        } catch(IOException | ClassNotFoundException e)
        {
            Cli.m(e.getMessage());
        } 
        return null;
    }
    
    /**
     * @package Assignment 10, Exercise 2 
     * @param grade
     * @throws IOException 
     */
    public void writeStudentsByAverageGrade(double grade) throws IOException 
    {
        // Get a list all of the students
        ArrayList<Student> list = new ArrayList<>();
        list.addAll(Student.getStaticStudentsList());
        list.addAll(TaStudent.getStaticStudentsList());
        list.addAll(PhdStudent.getStaticStudentsList());
        
        // Find not matching grades and remove them
        synchronized (list) {  
            for (Iterator it = list.iterator(); it.hasNext(); ) {  
                Student s = (Student) it.next();  
                if(s.averageNote() < grade)
                    it.remove();
            }  
        }  
        
        // Filename to save
        String filename = "stdata" + File.separator + "above_" + Math.abs(grade) + ".dat";
        // Storage folder
        File d = new File("stdata");
        // Create the dir if it diesn't exist
        if(!d.isDirectory()) {
            d.mkdir();
        }
        // Create fle object
        File f = new File(filename);
        // Create the fike on the disk if it doesn't exist
        if(!f.isFile())
        {
            f.createNewFile();
            MyLog.log(filename + " created in " + f.getPath(), MyLog.TO_CLI);
        }
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))) {
            out.writeObject(list);
            out.close();
            MyLog.log("Object " + list.toString() + " was written in " + f.getPath(), MyLog.TO_CLI);
        } catch(IOException | NullPointerException e)
        {
            MyLog.log(e.getCause().getMessage(), MyLog.TO_CLI);
        }
    }
}

/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

public class Constants {

    // General
    public static final String NEW_LINE         = System.getProperty("line.separator");
    public static final String CLI_WELCOME      = "Welcome!\n The following commands are available: {{::available_commands::}}.";
    public static final String CLI_OOR          = "The selected student ID doesn't exist.";
    public static final String CLI_NO_CMD       = "Command not found!";
    public static final String A4E5             = "Assignment 4, Exercise 5";
    public static final String QUEUE_CLASS      = "Queue Class (Assignment 6)";
    
    // Students Class
    public static final String NAME             = "Full Name";
    public static final String GROUP            = "Group";
    public static final String JAVA_PROF        = "Java Proficiency";
    public static final String CPR              = "CPR Number";
    public static final String GENDER           = "Gender (M/F)";
    public static final String GENDER_MALE      = "M";
    public static final String GENDER_FEMALE    = "F";
    public static final String DEFAULT_ST_GROUP = "ST";
    public static final String DEFAULT_TA_GROUP = "TA";
    public static final String DEFAULT_PHD_GROUP= "PHD";
    public static final String DEF_NEW_STD_MSG  = "New Student added! There is a total of {{:x:}} students";
    public static final String DEF_NEW_TA_MSG   = "New Teaching Assistant added! There is a total of {{:x:}} Teaching Assistants.";
    public static final String DEF_NEW_PHD_MSG  = "New PhD Student added! There is a total of {{:x:}} PhD. students";
    public static final String DEGREE_BSC       = "Bachelor's";
    public static final String DEGREE_MSC       = "Master's";
    public static final String DEGREE_PHD       = "Doctor of Philosophy";
    
    // Test students Class
    public static final String ADD              = "Add Students";
    public static final String COMPARE          = "Compare Students";
    public static final String STUDENT_TO_FILE  = "Save Students to File (Grade)";
    public static final String COMPARE_J        = "Compare Java Proficiency";
    public static final String SEARCH_P         = "Search by Proficiency";
    public static final String GRADES           = "Get student grades";
    public static final String CLONE_TEST       = "Clone Test";
    public static final String CLI_COMMANDS      = "CLI Commands";
    public static final String SAME_STUD        = "The two students are the same";
    public static final String DIFF_STUD        = "The two students are different";
    public static final String SAME_PROF        = "The two students have the same Java Proficiency";
    public static final String DIFF_PROF        = "The two students have different Java Proficiency";
    public static final String SELECT_ACTION_MSG= "Select action to do and press OK";
    public static final String SELECT_ACTION    = "What would you like to do?";
    public static final String COMP_STUD_LBL1   = "Student 1";
    public static final String COMP_STUD_LBL2   = "Student 2";
    public static final String NO_ACTION        = "Nothing to do";
    public static final String S_DESIRED_JVPRF  = "Enter the desired Java Proficiency value";
    public static final String TOTAL_RESULTS    = "Found {{:x:}} results matching criteria";
    public static final String NO_RESULTS       = "No results found";
    public static final String CLONED_OBJ_MSG   = "The cloned object is {{:x:}}";
    public static final String ADDSTUD_DL_LBL   = "Degree level";
    public static final String ADDSTUD_TA_LBL   = "Course support";
    public static final String SECONDS_LEFT_LBL = "You have 10s to start typing in the 'Full Name' field!";
    public static final String CTD_CANCELLED    = "Countdown cancelled";
    public static final String RANDW_TEST       = "Read and Write test";
    
    // Grades
    public static final String GPA              = "Average";
    public static final String TOTAL_COURSES    = "Total Courses";
    
    // Cli
    public static final String METHOD_NOT_FOUND = "No suitable method found for the requested command";
    public static final String HELP_MESSAGE     = "The command you typed is either incomplete or it does not exist.\n Please refer to the table below for help.\n";
    public static final String EXIT_HELP_MESSAGE= "Type 'EXIT' to close this window";
    public static final String HOW_MANY_ST_CR   = "How many students would you like to create?";
    public static final String EXCEPTION_IO_MSG = "There was an error in your input.";
    public static final String FIRST_NUMBER     = "Enter a";
    public static final String SECOND_NUMBER    = "Enter b";
    public static final String DIVIDE_RESULT    = "The division result is %s \n";
    
    // Exception Messages
    public static final String WRONG_NUMBER_FORMAT = "The value you are supposed to type must be a number";
    public static final String QUEUE_IS_FULL       = "The queue is already full";
    public static final String QUEUE_IS_EMPTY_EXC  = "The queue is empty.";
    
}

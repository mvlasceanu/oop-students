/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

import java.util.ArrayList;

public class PhdStudent extends Student implements Cloneable {
    
    /**
     * Instances counter
     */
    public static int COUNT_PHD = 0;
    
    /**
     * Constructor
     * 
     * @param newName
     * @param newProficiencyInJava
     * @param newCprNumber
     * @param newGender 
     */
    public PhdStudent(String newName, double newProficiencyInJava, String newCprNumber, String newGender) {
        super(newName, newProficiencyInJava, newCprNumber, newGender);
        this.setGroup(Constants.DEFAULT_PHD_GROUP);
    }

    /**
     * No param constructor
     */
    PhdStudent() {
    }
    
    /**
     * Override toString()
     * 
     * @packasge Assignment 4, exercise 4
     * @return 
     */
    @Override public String toString() {
        StringBuilder record = new StringBuilder();
        record.append(" " + Constants.NAME + ": ").append(this.getName()).append(Constants.NEW_LINE);
        record.append(" " + Constants.GROUP + ": ").append(this.getGroup()).append(Constants.NEW_LINE);
        record.append(" " + Constants.JAVA_PROF + ": ").append(this.getProficiencyInJava()).append(Constants.NEW_LINE);
        record.append(" " + Constants.CPR + ": ").append(this.getCprNumber()).append(Constants.NEW_LINE);
        record.append(" " + Constants.GENDER + ": ").append(this.getGender()).append(Constants.NEW_LINE);
        if(!"".equals(this.getDegreeLevel()))
        {
            record.append(" " + Constants.ADDSTUD_DL_LBL + ": ").append(this.getDegreeLevel()).append(Constants.NEW_LINE);
        }
        
        if(!this.getCourseSupport().isEmpty())
        {
            record.append(" " + Constants.ADDSTUD_TA_LBL + ": ").append(this.supportCoursesToString()).append(Constants.NEW_LINE);
        }
        return record.toString();
    }
    
    /**
     * Displays number of instances message in console
     */
    @Override
    public void getInstanceMessage()
    {
        COUNT_PHD++;
        MyLog.log(Constants.DEF_NEW_PHD_MSG.replace("{{:x:}}", "" + PhdStudent.COUNT_PHD + ""), MyLog.TO_CLI);
    }
    
    /**
     * Static students list
     * 
     * @return 
     */
    public static ArrayList getStaticStudentsList()
    {
        ArrayList<PhdStudent> PhdStudents = new ArrayList<>();
        // Fixed student objects        
        PhdStudents.add(new PhdStudent("Robert Sean Leonard", 7.5, "290190-1871", "M"));
        PhdStudents.add(new PhdStudent("Jesse Spencer",       7.5, "290190-1871", "M"));
        PhdStudents.add(new PhdStudent("Peter Jacobson",      2,   "110659-2564", "M"));
        PhdStudents.add(new PhdStudent("Olivia Wilde",        7.5, "120479-8547", "F"));
        PhdStudents.add(new PhdStudent("Kal Penn",            10,  "210566-6532", "M"));
       
       return PhdStudents;
    }
    
    /**
     * Override getGroup() to add Phd or TA.
     * @return 
     */
    @Override
    public String getGroup()
    {
        return this.group + " | " + this.getClass().getSimpleName();
    }
    
    /**
     * Override equals
     * 
     * @packasge Assignment 4, exercise 4
     * @param obj
     * @return 
     */
    @Override public boolean equals(Object obj) {
        return super.equals(obj);
    }
    
    /**
     * Override clone
     * 
     * @packasge Assignment 4, exercise 4
     * @return
     * @throws CloneNotSupportedException 
     */
    @Override protected PhdStudent clone() throws CloneNotSupportedException {
        return (PhdStudent) super.clone();
    }
    
    String p()
    {
        return "In";
    }
    /**
     * Assignment 4, exercise 5
     */
    @Override
    void display() { System.out.println("I am a PhD student"); }
}

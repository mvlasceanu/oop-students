/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assignment9
 * @class      Log
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.File;

/**
 * @package Assignment 10, Exercise 4,5 and 6
 * @author Mihai-Marius Vlăsceanu
 */
public class MyLog {
    // Log to file parameter
    public static final int TO_FILE     = 1;
    // Log to consle parameter
    public static final int TO_CLI      = 2;
    // Enable/Disable logging
    public static boolean LOG_ENABLED   = true;
    
    public static void log(String text, int destination)
    {   
        // Init Timestamp
        String timeStamp    = new SimpleDateFormat("yyyy-MM-dd H:m:s").format(Calendar.getInstance().getTime());
        // Init formatted date
        String dt           = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        // Init log file name
        String logFile      = "log" + File.separator + dt + ".log";
        // Init log file object
        File log            = new File(logFile);
        // Get the log folder
        File logFolder      = new File("log");
        // Create the folder if it doesn't exist
        if(!logFolder.isDirectory())
        {
            logFolder.mkdir();
        }
        // If the file doesn't exist
        if(log.isFile())
        {
            try {
                // Create it
                log .mkdir(); 
                log.createNewFile();
            } catch(IOException e)
            {
                Cli.m(e.getMessage());
            }
        }
        
        // Init formatted text
        String formatted = "%s - %s\n";
        
        // If log are enabled
        if(MyLog.LOG_ENABLED) {
            switch(destination)
            {
                case MyLog.TO_FILE:
                    // Wite the actual log text
                    try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(log, true)))) {
                        out.format(formatted, timeStamp, text);
                    } catch (IOException e) {
                        Cli.m(e.getMessage());
                    }
                    break;
                // Log to cli by default
                default:
                    Cli.f(formatted, timeStamp, text);
                    break;
            }
            
        }
        
    }
}

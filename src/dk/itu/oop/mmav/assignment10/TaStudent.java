/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

import java.util.ArrayList;

public class TaStudent extends Student implements Cloneable{
    /**
     * Instances counter
     */
    public static int COUNT_TA = 0;
    
    private ArrayList<TaStudent> static_students = new ArrayList<>();
    
    /**
     * Constructor
     * 
     * @param newName
     * @param newProficiencyInJava
     * @param newCprNumber
     * @param newGender 
     */
    public TaStudent(String newName, double newProficiencyInJava, String newCprNumber, String newGender) {
        super(newName, newProficiencyInJava, newCprNumber, newGender);
        this.setGroup(Constants.DEFAULT_TA_GROUP);
    }
    
    /**
     * No param constructor
     */
    TaStudent() {
       
    }
    
    public void addStaticStudent(Object s)
    {
        this.static_students.add((TaStudent) s);
    }
    
    public ArrayList<TaStudent> getStaticStudents()
    {
        return this.static_students;
    }
    
    /**
     * Override instance message
     */
    @Override
    public void getInstanceMessage()
    {
        COUNT_TA++;
        MyLog.log(Constants.DEF_NEW_TA_MSG.replace("{{:x:}}", "" + TaStudent.COUNT_TA + ""), MyLog.TO_CLI);
    }
    
    /**
     * Static students list
     * 
     * @return 
     */
    public static ArrayList getStaticStudentsList()
    {
        ArrayList<TaStudent> TaStudents = new ArrayList<>();
        // Fixed student objects
        TaStudents.add(new TaStudent("Bobbin Bergstrom", 7.5, "290190-1871", "F"));
        TaStudents.add(new TaStudent("Odette Annable",   7.5, "290190-1871", "F"));
        TaStudents.add(new TaStudent("Charlyne Yi",      2,   "110659-2564", "F"));
        TaStudents.add(new TaStudent("Anne Dudek",       7.5, "120479-8547", "F"));
        TaStudents.add(new TaStudent("Amber Tamblyn",    10,  "210566-6532", "F"));
       
       return TaStudents;
    }
    
    /**
     * Override toString()
     * 
     * @packasge Assignment 4, exercise 3
     * @return 
     */
    @Override public String toString() {
        StringBuilder record = new StringBuilder();
        record.append(" " + Constants.NAME + ": ").append(this.getName()).append(Constants.NEW_LINE);
        record.append(" " + Constants.GROUP + ": ").append(this.getGroup()).append(Constants.NEW_LINE);
        record.append(" " + Constants.JAVA_PROF + ": ").append(this.getProficiencyInJava()).append(Constants.NEW_LINE);
        record.append(" " + Constants.CPR + ": ").append(this.getCprNumber()).append(Constants.NEW_LINE);
        record.append(" " + Constants.GENDER + ": ").append(this.getGender()).append(Constants.NEW_LINE);
        if(!"".equals(this.getDegreeLevel()))
        {
            record.append(" " + Constants.ADDSTUD_DL_LBL + ": ").append(this.getDegreeLevel()).append(Constants.NEW_LINE);
        }
        record.append(" " + Constants.ADDSTUD_TA_LBL + ": ").append(this.supportCoursesToString()).append(Constants.NEW_LINE);
        return record.toString();
    }
    
    /**
     * Override getGroup() to add TA
     * @return 
     */
    @Override
    public String getGroup()
    {
        return this.group + " | " + this.getClass().getSimpleName();
    }
    
    /**
     * Override equals
     * 
     * @packasge Assignment 4, exercise 3
     * @param obj
     * @return 
     */
    @Override public boolean equals(Object obj) {
        return super.equals(obj);
    }
    
    /**
     * Override clone()
     * 
     * @packasge Assignment 4, exercise 3
     * @return
     * @throws CloneNotSupportedException 
     */
    @Override protected TaStudent clone() throws CloneNotSupportedException {
        return (TaStudent) super.clone();
    }
    
    /**
     * @packasge Assignment 4, exercise 4
     */
    @Override
    void display() { System.out.println("I am a TA"); }
}

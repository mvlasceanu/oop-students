/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @class      MyStringQueue
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

import dk.itu.oop.mmav.assignment9.exceptions.QueueIsEmptyException;
import dk.itu.oop.mmav.assignment9.exceptions.QueueOverflowException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 
 * @author Mihai-Marius Vlăsceanu
 * @package Assignment 6
 * 
 */
public class MyStringQueue {
    // Maximum size of the queue
    private int maxSize = 4;
    // Store the queue here
    private ArrayList<String> queue = new ArrayList<>();
    
    /**
     * Parameterless constructor
     */
    public MyStringQueue(){};
    
    /**
     * Constructor with a list
     * @param list
     * @throws QueueOverflowException 
     */
    public MyStringQueue(ArrayList list) throws QueueOverflowException
    {
        this.setQueue(list);
    }
    
    /**
     * Setter for maximum size
     * @param s 
     */
    public void setMaxSize(int s)
    {
        this.maxSize = s;
    }
    
    /**
     * Getter for maximum size
     * @return 
     */
    public int getMaxSize()
    {
        return this.maxSize;
    }
    
    /**
     * Setter for the queue
     * @param list
     * @throws QueueOverflowException 
     */
    public final void setQueue(ArrayList list) throws QueueOverflowException
    {
        if(list.size() <= this.getMaxSize())
        {
            this.queue = list;
        } else
            throw new QueueOverflowException();
        
    }
    
    /**
     * Queue getter
     * @return 
     */
    public ArrayList<String> getQueue()
    {
        return this.queue;
    }
    
    /**
     * Add element to queue and remove the first element
     * if the queue is full
     * @param e
     * @return 
     * @throws dk.itu.oop.mmav.assigment4.exceptions.QueueIsEmptyException 
     */
    public boolean add(String e) throws QueueIsEmptyException {
        boolean added = this.getQueue().add(e);
        while (added && this.getQueue().size() > this.getMaxSize()) {
           this.remove();
        }
        return added;
    }
    
    /**
     * Try to add element in the queue and throw an 
     * exception if the queue is full
     * 
     * @param e
     * @param removeIfFull
     * @return
     * @throws QueueOverflowException 
     * @throws dk.itu.oop.mmav.assignment9.exceptions.QueueIsEmptyException 
     */
    public boolean add(String e, boolean removeIfFull) throws QueueOverflowException, QueueIsEmptyException
    {
        if(!removeIfFull)
            if(this.isFull())
                throw new QueueOverflowException(Constants.QUEUE_IS_FULL);
        return this.add(e);
    }
    
    /**
     * Check if the queue is full
     * @return 
     */
    public boolean isFull()
    {
        return this.getQueue().size() == this.getMaxSize();
    }
    
    /**
     * Removes the head element form the queue 
     * 
     * @return 
     * @throws dk.itu.oop.mmav.assignment9.exceptions.QueueIsEmptyException 
     */
    public String remove() throws QueueIsEmptyException 
    {
        if(!this.getQueue().isEmpty())
        {
            String head = this.getQueue().get(0);
            this.getQueue().remove(head);
            return head;
        } else 
            throw new QueueIsEmptyException(Constants.QUEUE_IS_EMPTY_EXC);
    }
    
    /**
     * Removes the head element form the queue and it
     * returns it
     * @return 
     */
    public String poll() 
    {
        if(!this.getQueue().isEmpty())
        {
            String head = this.getQueue().get(0);
            this.getQueue().remove(head);
            return head;
        } else 
            return null;
    }
    
    /**
     * Returns the current element in the queue while throwing
     * an exception if the queue is empty
     * @return 
     * @throws dk.itu.oop.mmav.assignment9.exceptions.QueueIsEmptyException 
     */
    public String element() throws QueueIsEmptyException {
        if(!this.getQueue().isEmpty())
            return this.getQueue().get(0);
        else throw new QueueIsEmptyException(Constants.QUEUE_IS_EMPTY_EXC);
    }
    
    /**
     * Retrieves the first element in the queue without
     * removing it, while returning NULL if the queue is
     * empty
     * 
     * @return 
     */
    public String peek() {
        if(!this.getQueue().isEmpty())
            return this.getQueue().get(0);
        else 
            return null;
    }
    
    /**
     * Returns the current number of elements in the queue
     * @return 
     */
    public int size() {
        return this.getQueue().size();
    }

    /**
     * Checks if there are no elements in the queue
     * @return 
     */
    public boolean isEmpty() {
        return this.getQueue().isEmpty();
    }

    /**
     * Checks if the queue already contains an
     * elements to be added
     * @param o
     * @return 
     */
    public boolean contains(String o) {
        return this.getQueue().contains(o);
    }
    
    /**
     * Returns an iterator over the elements in the queue
     * @return 
     */
    public Iterator<String> iterator() {
       return this.getQueue().iterator();
    }
    
    /**
     * Converts the queue into an array containing
     * queue's elements
     * @return 
     */
    public Object[] toArray() {
        return this.getQueue().toArray();
    }
    
    /**
     * Removes a single instance of the specified 
     * parameter
     * @param o
     * @return 
     */
    public boolean remove(String o) {
        for(String s : this.getQueue())
        {
            if(s.equals(o))
            {
                String r = s;
                int index = this.getQueue().indexOf(s);
                this.getQueue().remove(index);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Checks if the current queue contains all the elements
     * within the specified collection
     * @param c
     * @return 
     */
    public boolean containsAll(Collection<?> c) {
        return this.getQueue().containsAll(c);
    }
    
    /**
     * Adds all of the elements in the specified 
     * collection to this queue
     * @param c
     * @return 
     */
    public boolean addAll(Collection c) {
        return this.getQueue().addAll(c);
    }
    
    /**
     * The opposite of the addAll() method
     * @param c
     * @return 
     */
    public boolean removeAll(Collection c) {
        return this.getQueue().removeAll(c);
    }
    
    /**
     * It removes all the elements from this queue 
     * that are outside of the elements of the specified collection
     * @param c
     * @return 
     */
    public boolean retainAll(Collection<?> c) {
        return this.getQueue().retainAll(c);
    }
    
    /**
     * Empties the current queue
     */
    public void clear() {
        this.getQueue().clear();
    }
    
    /**
     * Displays the queue in a string format
     * @return 
     */
    @Override
    public String toString()
    {
        StringBuilder record = new StringBuilder();
        for(String s : this.getQueue())
        {
            int index = this.getQueue().indexOf(s);
            record.append(index)
                  .append(": ")
                  .append(s)
                  .append(Constants.NEW_LINE);
        }
        
        return record.toString();
    }
}

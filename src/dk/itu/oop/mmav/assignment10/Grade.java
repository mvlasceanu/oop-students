/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

import java.io.Serializable;

public final class Grade implements Serializable {
    public String Course;
    private float Value;
    
    Grade(String Course, float Value)
    {
        this.setCourse(Course);
        this.setValue(Value);
    }
    
    public void setCourse(String name)
    {
        this.Course = name;
    }
    public String getCourse()
    {
        return this.Course;
    }
    public void setValue(float value)
    {
        this.Value = value;
    }
    public float getValue()
    {
        return this.Value;
    }
}

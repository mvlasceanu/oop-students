/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public final class Cli {

    // Store the requested command
    private String command;
    // Store the mapped method
    private String method;
    // Store the params for the method
    private String params;
    public Object data;
    private int arg;
    
    /**
     * Constructor
     * 
     * @param command 
     */
    Cli(String command) {
        this.setCommand(command);
        this.mapCommandToMethod();
    }
    
    /**
     * No arguments constructor
     * @param welcome
     */
    public Cli(boolean welcome)
    {
        if(welcome == true)
            this.displayWelcomeMessage();
    }
    
    /**
     * Set the command types by the user
     * 
     * @param command 
     */
    public void setCommand(String command)
    {
        this.command = command;
    }
    
    /**
     * Get the command typed by the user
     * 
     * @return 
     */
    public String getCommand()
    {
        return this.command;
    }
    
    /*
     * Single int argument getter 
     *
     * @return 
     */
    public int getArg()
    {
        return this.arg;
    }
    
    /**
     * Argument setter (only for single int methods)
     * 
     * @param arg 
     */
    public void setArg(int arg)
    {
        this.arg = arg;
    }
    
    /**
     * Set method's params
     * 
     * @param param 
     */
    public void setParams(String param)
    {
        this.params = param;
    }
    
    /**
     * Retrieve method's params
     * 
     * @return 
     */
    public String getParams()
    {
        return this.params;
    }
    
    /**
     * Method param setter
     * 
     * @param method 
     */
    public void setMethod(String method)
    {
        this.method = method;
    }
    
    /**
     * Get the name of the method that should
     * be called, according with the command
     * 
     * @return 
     */
    public String getMethodName()
    {
        return this.method;
    }
    
    /**
     * Maps the input command to a spcific method
     */
    public void mapCommandToMethod() throws SecurityException
    {
        try
        {
            if(this.getCommand().length() >= 2 && ("TA".equals(this.getCommand().substring(0, 2)) || "ST".equals(this.getCommand().substring(0, 2))))
            {
                // Commands only for TA, PHD, ST
                String str2 = this.getCommand().substring(0, 2);
                 // If it is TA or ST
                if(this.isValidCommand(str2))
                {
                    switch(str2)
                    {
                        case Constants.DEFAULT_ST_GROUP:
                        {
                            this.setArg(Integer.parseInt(this.getCommand().substring(2)));
                            this.setMethod("execSt");
                        }
                        case Constants.DEFAULT_TA_GROUP:
                        {
                            this.setArg(Integer.parseInt(this.getCommand().substring(2)));
                            this.setMethod("execTa");
                        }
                        default:
                            m(Constants.CLI_NO_CMD);
                    }
                }
            }
            if(this.getCommand().length() >= 3 && "PHD".equals(this.getCommand().substring(0, 3))){
                String str3 = this.getCommand().substring(0, 3);
                // If the command is PHD
                if(this.isValidCommand(str3))
                {
                    this.setMethod("execPhd");
                }
            }

            // If the command is other than the onbes above
            // Part of Assignment 5, Exercise 5
            if(this.isValidCommand(this.getCommand()))
            {
                String cmd  = this.getCommand();
                String p2   = this.camelCase(cmd);
                String method_to_call = "exec" + p2;
                if(!this.methodExists(method_to_call))
                {
                    m(Constants.METHOD_NOT_FOUND + "( " + method_to_call + ")");
                } else {
                    this.setMethod(method_to_call);  
                }
            } else {
                this.runHelp();
            }
        } catch(NumberFormatException E)
        {
            m(E.getMessage());
        }
        
    }
    
    /**
     * Check if the requested method actually
     * exists in the current class
     * 
     * @param name
     * @return 
     */
    public boolean methodExists(String name)
    {
        boolean hasMethod = false;
        Method[] methods = this.getClass().getMethods();
        for (Method m : methods) {
          if (m.getName().equals(name)) {
            hasMethod = true;
            break;
          }
        }
        return hasMethod;
    }
    /**
     *
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public void execute() throws SecurityException, NoSuchMethodException, IllegalArgumentException,
            IllegalAccessException, InvocationTargetException {
        try
        {
            if(this.getMethodName() != null && this.methodExists(this.getMethodName()))
            {
                Method m = this.getClass().getMethod(this.getMethodName(), new Class[] {});
                Object ret;
                ret = m.invoke(this, new Object[] {});
            }
        } catch(IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e)
        {
            m(e.getMessage());
        }
        
    }
    /**
     * An array of strings with valid commands
     * 
     * @return
     */
    public String[] getValidCommands()
    {
        String[] cmds = {"PHD", "TA", "ST", "ADD_STUDENT", "DIVIDE", "CREATE_ARRAY", "HELP"};
        return cmds;
    }
    
    /**
     * Check if the typed command is defined
     * 
     * @param command
     * @return 
     */
    public boolean isValidCommand(String command)
    {
        String[] valids = this.getValidCommands();
        for(String c : valids )
        {
            if(c != null && command != null && c.equals(command))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Displays a message with the available commands
     */
    public void displayWelcomeMessage()
    {
        String message = Constants.CLI_WELCOME;
        String[] cmds     = this.getValidCommands();
        String pcommand   = "";
        int counter       = 1;
        int cmdsz         = cmds.length;
        for(String cmd : cmds)
        {
            pcommand += cmd.toString();
            if(counter < cmdsz)
                pcommand += ", ";
            counter++;
        }
        message = message.replace("{{::available_commands::}}", pcommand);
        m( message );
    }
    
    /**
     * Check is the typed command is designed to
     * have parameters/arguments
     * 
     * @param cmd
     * @return 
     */
    public boolean isCommandWIthParams(String cmd)
    {
        String[] chunks = cmd.split("\\s+");
        String first    = chunks[0];
        switch(first)
        {
            case "ADD_STUDENT":
                return true;
            default:
                return false;
        }
    }
    
    /**
     * Parses the command with parameters
     * 
     * @package Assignment 5, Exercise 5
     * @param command 
     */
    public void parseParameterdCommand(String command) throws StringIndexOutOfBoundsException
    {
        try
        {
            String[] chunks = command.split("\\s+");
            String first    = chunks[0];
            this.setCommand(first);
            this.params     = command.substring(first.length() + 1, command.length());   
        } catch(Exception E)
        {
            m(E.getMessage());
        }
        
    }
    
    /**
     * Helper method to output for 'HELP' command
     * 
     * @param command
     * @return 
     */
    public String getCommandParams(String command)
    {
        switch(command)
        {
            case "PHD":
            case "TA":
            case "ST" :
                return "<id>";
            case "ADD_STUDENT":
                return "<name> <java proficiency> <cpr> <gender>";
            case "HELP":
            case "CREATE_ARRAY":
                return "-";
            default:
                return "-";
        }
    }
    
    public void execHelp()
    {
        System.out.format("%10s%15s", "Command", "Parameter");
        m("");
        for(String cmd : this.getValidCommands())
        {
            String c = cmd;
            String p = this.getCommandParams(c);
            System.out.format("%10s%15s", c, p);
            m("");
        }
    }
    
    public void runHelp()
    {
        m(Constants.HELP_MESSAGE);
        this.execHelp();
    }
    
    public void execPhd()
    {
        ArrayList<PhdStudent> list;
        if(this.data == null) {
            list    = PhdStudent.getStaticStudentsList();
            this.data = list;
            PhdStudent.COUNT_PHD = 0;
        } else {
            list      = (ArrayList<PhdStudent>) this.data;
        }
        try
        {
            if(this.getArg() <= (list.size() - 1))
            {
                PhdStudent record   = list.get(this.getArg());
                m(record.toString());
            } else {
                m(Constants.CLI_OOR);
            }

        }
        catch(NumberFormatException E)
        {
            m(E.getMessage());
        }
    }
    
    public void execSt()
    {
        ArrayList<Student> list;
        if(this.data == null) {
            list    = Student.getStaticStudentsList();
            this.data = list;
            Student.COUNT_ST = 0;
        } else {
            list      = (ArrayList<Student>) this.data;
        }
        try
        {
            if(this.getArg() <= (list.size() - 1))
            {
                Student record   = list.get(this.getArg());
                m(record.toString());
            } else {
                m(Constants.CLI_OOR);
            }

        }
        catch(NumberFormatException E)
        {
            m(E.getMessage());
        }
    }
    
    public void execTa()
    {
        ArrayList<TaStudent> list;
        if(this.data == null) {
            list    = TaStudent.getStaticStudentsList();
            this.data = list;
            TaStudent.COUNT_TA = 0;
        } else {
            list      = (ArrayList<TaStudent>) this.data;
        }

        try
        {
            if(this.getArg() <= (list.size() - 1))
            {
                TaStudent record   = list.get(this.getArg());
                m(record.toString());
            } else {
                m(Constants.CLI_OOR);
            }

        }
        catch(NumberFormatException E)
        {
            m(E.getMessage());
        }
    }
    
    /**
     * Creates a new student via Command LINE
     * 
     * @return 
     * @package Assignment 5, Exercise 5
     */
    public Student execAddStudent()
    {
       try {
           // Get the params string
           String params        = this.getParams();
           // Split the string into pieces
           String[] chunks      = params.split("\\s+");
           // Student name
           String name          = chunks[0].trim();
           // Java Proficiency
           Double proficiency   = Double.parseDouble(chunks[1].trim());
           // Cpr
           String cpr           = chunks[2].trim();
           // Gender
           String gender        = chunks[3].trim();
           
           // Validate gender
           if(!Student.isValidGender(gender)) {
               System.out.format("Invalid gender %10", gender);
           } else {
                // Create student
                return new Student(name, proficiency, cpr, gender);
           }
        } catch(NumberFormatException e)
        {
            m(e.getMessage());
        }
        return null;
    }
    
    /**
     * Ads students to a fixed size array
     * 
     * @package Assignment 5, Exercise 6
     */
    public void execCreateArray()
    {
        try
        {
            // Init input window
            InputStreamReader input = new InputStreamReader(System.in);
            // Store the types stuff in memory
            BufferedReader reader = new BufferedReader(input);
            // Total students question
            m(Constants.HOW_MANY_ST_CR);
            // Get input
            int t = Integer.parseInt(reader.readLine().trim());
            // Init array
            Student[] catalog = new Student[t];
            // Add students
            for(int i = 0; i < t; i++)
            {
                // Get name
                m(Constants.NAME);
                String name = reader.readLine();
                // Get Java Proficiency
                m(Constants.JAVA_PROF);
                Double jp = Double.parseDouble(reader.readLine());
                // Get CPR number
                m(Constants.CPR);
                String cpr = reader.readLine();
                // Get gender
                m(Constants.GENDER);
                String gender = reader.readLine();
                // Add to array
                catalog[i]      = new Student(name, jp, cpr, gender);
            }
        }catch(IOException | NumberFormatException E)
        {
            if(E instanceof NumberFormatException)
                m(Constants.EXCEPTION_IO_MSG + " " + Constants.WRONG_NUMBER_FORMAT);
            else
                m(Constants.EXCEPTION_IO_MSG);
        }
        
    }
    
    /**
     * Executes division between a and b
     * 
     * @package Assignment 5, Exercise 7
     */
    public void execDivide()
    {
        try
        {
            // Init input window
            InputStreamReader input = new InputStreamReader(System.in);
            // Store the types stuff in memory
            BufferedReader reader = new BufferedReader(input);
            // Total students question
            m(Constants.FIRST_NUMBER);
            // Get input
            int a = Integer.parseInt(reader.readLine().trim());
            // Total students question
            m(Constants.SECOND_NUMBER);
            // Get input
            int b = Integer.parseInt(reader.readLine().trim());
            // Message
            f(Constants.DIVIDE_RESULT, a/b);
        } catch(IOException | NumberFormatException | ArithmeticException E)
        {
            m(E.getMessage());
        }
    }
    
    /**
     * Shortcut to m
     * 
     * @param message 
     */
    public static void m(String message)
    {
        System.out.println(message);
    }
    
    public static void f(String message, Object... args)
    {
        System.out.format(message, args);
    }
    
    /**
     * Converts commands to camel case strings
     * so it can be matched easily to a method
     * 
     * @param str
     * @return 
     */
    public String camelCase(String str)
    {
        String[] camels = str.split("\\_");
        String final_method_name = "";
        for (String camel : camels) {
            final_method_name += camel.substring(0, 1).toUpperCase() + camel.substring(1).toLowerCase();
        }
        return final_method_name;
    }
}

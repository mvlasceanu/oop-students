/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment10;
import dk.itu.oop.mmav.assignment9.exceptions.QueueIsEmptyException;
import dk.itu.oop.assignment11.mmav.MyMaskForInputStream;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.*;  
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class TestStudent {
   
   
    /**
     * @param args the command line arguments
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.reflect.InvocationTargetException
     * @throws java.lang.InstantiationException
     * @throws dk.itu.oop.mmav.assignment9.exceptions.QueueIsEmptyException
     * @throws java.io.IOException
     */
    
    // The "static" word is like a signature for java. Java looks for rhis specific word
    // to run the application and to make the difference between its' main class and other 
    // classes's main method. Therefore, removing the "static" word will cause the application
    // not to run.
    public static void main(String[] args) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException, QueueIsEmptyException, IOException {
        
        int ttlStudents = 5;
        // Predefined actions list
        String[] actionsList = {Constants.ADD, 
                                Constants.STUDENT_TO_FILE,
                                Constants.RANDW_TEST,
                                Constants.COMPARE, 
                                Constants.COMPARE_J,
                                Constants.SEARCH_P,
                                Constants.GRADES,
                                Constants.CLONE_TEST,
                                Constants.CLI_COMMANDS,
                                Constants.A4E5,
                                Constants.QUEUE_CLASS
                                };
        JComboBox action     = new JComboBox(actionsList);
        JPanel actionPanel   = new JPanel(new GridLayout(0,2));
        actionPanel.add(new JLabel(Constants.SELECT_ACTION));
        actionPanel.add(action);
        
        try {
            
            // fetch selected action
            int doAction = JOptionPane.showConfirmDialog(null, actionPanel, 
                         Constants.SELECT_ACTION, JOptionPane.OK_CANCEL_OPTION);
            
            // If the OK button is pressed
            if(doAction == JOptionPane.OK_OPTION)
            {
                // Get the actual selected action
                String a = String.valueOf(action.getSelectedItem());

                    // If the action is to compare the objects
                    switch (a) {
                        case Constants.COMPARE:
                            {
                                // Lists for the dropdown
                                ArrayList<?> list = Student.getStaticStudentsList();
                                list.addAll(TaStudent.getStaticStudentsList());
                                list.addAll(PhdStudent.getStaticStudentsList());
                                JComboBox l1 = new JComboBox(list.toArray());
                                JComboBox l2 = new JComboBox(list.toArray());
                                TestStudent Student = new TestStudent();
                                Student.initComparePanel(list);
                                break;
                            }
                        case Constants.COMPARE_J:
                            {
                                 // Lists for the dropdown
                                ArrayList<Student> list = Student.getStaticStudentsList();

                                JComboBox l1 = new JComboBox(list.toArray());
                                JComboBox l2 = new JComboBox(list.toArray());
                                TestStudent Student = new TestStudent();
                                Student.initCompareJavaPanel(list);
                                break;
                            }
                        case Constants.SEARCH_P:
                        {
                                // Lists for the dropdown
                                ArrayList<Student> list = Student.getStaticStudentsList();
                                list.addAll(TaStudent.getStaticStudentsList());
                                list.addAll(PhdStudent.getStaticStudentsList());
                                JComboBox l1 = new JComboBox(list.toArray());
                                JComboBox l2 = new JComboBox(list.toArray());
                                TestStudent Student = new TestStudent();
                                Student.search(list);
                                break;
                        }
                        case Constants.GRADES:
                        {
                             // Lists for the dropdown
                            ArrayList<Student> list = Student.getStaticStudentsList();

                            JComboBox l1 = new JComboBox(list.toArray());
                            JComboBox l2 = new JComboBox(list.toArray());
                            TestStudent Student = new TestStudent();
                            Student.getGrades(list);
                            break;
                        }
                        case Constants.CLONE_TEST:
                        {
                             // Lists for the dropdown
                            ArrayList<Student> list = Student.getStaticStudentsList();

                            JComboBox l1 = new JComboBox(list.toArray());
                            JComboBox l2 = new JComboBox(list.toArray());
                            TestStudent Student = new TestStudent();
                            Student.doCloneTest(list);
                            break;
                        }
                        case Constants.CLI_COMMANDS:
                        {
                            TestStudent t = new TestStudent();
                            t.performCliCommands(true);
                            break;
                        }
                        case Constants.A4E5:
                        {
                            TestStudent t = new TestStudent();
                            t.a4e5();
                            break;
                        }
                        case Constants.QUEUE_CLASS:
                        {
                            TestStudent t = new TestStudent();
                            t.queueClassTest();
                            break;
                        }
                        case Constants.STUDENT_TO_FILE:
                        {
                            TestStudent t = new TestStudent();
                            t.saveStudentsByGrade();
                            break;
                        }
                        case Constants.RANDW_TEST:
                        {
                            TestStudent t = new TestStudent();
                            t.readAndWriteTest();
                            break;
                        }
                            
                        default:
                        {
                            /**
                             * Part of Assignment 9
                             */
                            //Use the event dispatch thread for Swing components
                            EventQueue.invokeLater(new Runnable()
                            {

                               @Override
                                public void run()
                                {
                                    TestStudent Students = new TestStudent();
                                   try {
                                       Students.addStudents();
                                   } catch (IOException | ClassNotFoundException ex) {
                                       Cli.m(ex.getMessage());
                                       
                                   }
                                }
                            });
                            break;
                        }
                    } 
                }
        } catch(HeadlessException | NumberFormatException E)
        {
            System.out.println(E.getMessage());
        }
    }
    
    /**
     * Compares if two objects are the same
     * 
     * @param list
     */
    public void initComparePanel(ArrayList<?> list)
    {
        JComboBox l1 = new JComboBox(list.toArray());
        JComboBox l2 = new JComboBox(list.toArray());
        JPanel comparePanel   = new JPanel(new GridLayout(0,2));
        comparePanel.add(new JLabel(Constants.COMP_STUD_LBL2));
        comparePanel.add(l1);
        comparePanel.add(new JLabel(Constants.COMP_STUD_LBL1));
        comparePanel.add(l2);
        // Display choices
        int comp = JOptionPane.showConfirmDialog(null, comparePanel,
                Constants.COMPARE, JOptionPane.OK_CANCEL_OPTION);
        Student index1 = (Student) list.get(l1.getSelectedIndex());
        Student index2 = (Student) list.get(l2.getSelectedIndex());
        // Compare and display message/result
        if(comp == JOptionPane.OK_OPTION)
        {
            if(index1.equals(index2))
            {
                JOptionPane.showMessageDialog(null, Constants.SAME_STUD);
            } else {
                JOptionPane.showMessageDialog(null, Constants.DIFF_STUD);
            }
        }
    }
    
    /**
     * Displays the comparison dialog for java proficiency
     * 
     * @param list 
     */
    public void initCompareJavaPanel(ArrayList<?> list)
    {
        JComboBox l1 = new JComboBox(list.toArray());
        JComboBox l2 = new JComboBox(list.toArray());
        JPanel comparePanel   = new JPanel(new GridLayout(0,2));
        comparePanel.add(new JLabel(Constants.COMP_STUD_LBL2));
        comparePanel.add(l1);
        comparePanel.add(new JLabel(Constants.COMP_STUD_LBL1));
        comparePanel.add(l2);
        int comp = JOptionPane.showConfirmDialog(null, comparePanel,
                                    Constants.COMPARE_J, JOptionPane.OK_CANCEL_OPTION);
        Student index1 = (Student) list.get(l1.getSelectedIndex());
        Student index2 = (Student) list.get(l2.getSelectedIndex());
        String jp_displayed_value = " (" + index1.getProficiencyInJava() + " - " + index2.getProficiencyInJava() + ") ";
        if(index1.hasSameFluencyInJavaAs(index2) == true)
        {
            JOptionPane.showMessageDialog(null, Constants.SAME_PROF + jp_displayed_value);
        } else {
            JOptionPane.showMessageDialog(null, Constants.DIFF_PROF + jp_displayed_value);
        }       
    }
    
    /**
     * Performs the search through static students by Java 
     * proficiency input criteris
     * 
     * @param list 
     */
    public void search(ArrayList<Student> list)
    {
        ArrayList<Student> res = new ArrayList<>();
        double search = Double.parseDouble(JOptionPane.showInputDialog(Constants.S_DESIRED_JVPRF));
        try {
            int i = 0;
            for(Student s : list)
            {
                if(s.getProficiencyInJava() == search)
                {
                    res.add(s);
                    i++;
                }
            }
            
            if(!res.isEmpty())
            {
                String ttl     = "" + res.size() + "";
                String results = Constants.TOTAL_RESULTS.replace("{{:x:}}", ttl);
                StringBuilder resultItems = new StringBuilder();
                resultItems.append(results).append(Constants.NEW_LINE);
                for(Student st : res)
                {
                    resultItems.append(st.toString()).append(Constants.NEW_LINE);
                }
                JOptionPane.showMessageDialog(null, resultItems.toString());
            } else {
                JOptionPane.showMessageDialog(null, Constants.NO_RESULTS);
            }
        } catch(HeadlessException E) {
            JOptionPane.showMessageDialog(null, E.getMessage());
        }
    }
    
    /**
     * Fetches the given student's grades
     * 
     * @param list 
     */
    public void getGrades(ArrayList<Student> list)
    {
         // Lists for the dropdown
        JPanel gradesPanel   = new JPanel(new GridLayout(0,2));
        JComboBox l1 = new JComboBox(list.toArray());
        gradesPanel.add(new JLabel(Constants.COMP_STUD_LBL1));
        gradesPanel.add(l1);

        // Display choices
        int stud = JOptionPane.showConfirmDialog(null, gradesPanel, 
             Constants.GPA, JOptionPane.OK_CANCEL_OPTION);
        Student index = list.get(l1.getSelectedIndex());

        JOptionPane.showMessageDialog(null, index.getStudentGradesString());

        System.exit(0);
    }
    
    /**
     * Object clone test
     * 
     * @param list 
     */
    public void doCloneTest(ArrayList<Student> list)
    {
        JPanel gradesPanel   = new JPanel(new GridLayout(0,2));
        JComboBox l1 = new JComboBox(list.toArray());
        gradesPanel.add(new JLabel(Constants.COMP_STUD_LBL1));
        gradesPanel.add(l1);
        try
        {
            // Display choices
            int stud                    = JOptionPane.showConfirmDialog(null, gradesPanel, 
                 Constants.CLONE_TEST, JOptionPane.OK_CANCEL_OPTION);

            Student index               = list.get(l1.getSelectedIndex());

            Student cloned              = index.clone();
            String objstr               = cloned.toString();
            String msg                  = Constants.CLONED_OBJ_MSG.replace("{{:x:}}", Constants.NEW_LINE + objstr);
            StringBuilder cloned_msg    = new StringBuilder();

            cloned_msg.append(msg).append(Constants.NEW_LINE);

            JOptionPane.showMessageDialog(null, cloned.toString());

            System.exit(0);
        } catch(CloneNotSupportedException E) {
            JOptionPane.showMessageDialog(null, "Error: " + E.getMessage());
        } finally {
            System.exit(0);
        }
    }
    
    /**
     * Manually add and display students from dialog
     * @throws java.io.IOException
     * @throws java.io.FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    public void addStudents() throws IOException, FileNotFoundException, ClassNotFoundException
    {
        // The following code uses the JOptionPane interaction.
        // Thus, students are inserted manually, one by one
        int ttlStudents = 5;

        // Dinamically create student objects
        for(int i = 1; i <= ttlStudents; i++)
        {
            /**
             * Part of Assignment 9
             */
            final Thread WARNING = new Thread(new WarningThread());
            WARNING.start();
            
            final JLabel timeLeft = new JLabel(Constants.SECONDS_LEFT_LBL);
            timeLeft.setBackground(Color.red);
            timeLeft.setForeground(Color.red);
            
            // Form fields
            JTextField newName          = new JTextField(10);
            
            newName.addKeyListener(new KeyListener()
            {
                @Override
                public void keyTyped(KeyEvent e) {
                    if(WARNING.isAlive())
                    {
                        WARNING.interrupt();
                        //JOptionPane.showMessageDialog(null, Constants.CTD_CANCELLED);
                        timeLeft.setText("");
                    }
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    if(WARNING.isAlive())
                    {
                        WARNING.interrupt();
                        //JOptionPane.showMessageDialog(null, Constants.CTD_CANCELLED);
                        timeLeft.setText("");
                    }
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    if(WARNING.isAlive())
                    {
                        WARNING.interrupt();
                        //JOptionPane.showMessageDialog(null, Constants.CTD_CANCELLED);
                        timeLeft.setText("");
                    }
                }
            });
            /**
             * Part of Assignment 9 End
             */
            JTextField newGroup         = new JTextField(2);
            JTextField newProficiency   = new JTextField(4);
            JTextField newCpr           = new JTextField(11);
            String[] genderList         = {Constants.GENDER_MALE, Constants.GENDER_FEMALE};
            JComboBox newGender         = new JComboBox(genderList);
            JComboBox degreeLevels      = new JComboBox(Student.getDegreeLevels());
            JList courses               = new JList(Student.getPossibleCourses());
            JScrollPane scrollCourses   = new JScrollPane();
            scrollCourses.setViewportView(courses);
            
            // Panel UI
            GridBagLayout GUI = new GridBagLayout();
            GridBagConstraints con = new GridBagConstraints();
            // Add fields to Panel
            JPanel StudentPanel      = new JPanel(GUI);
            con.fill = GridBagConstraints.HORIZONTAL;
            con.weightx = 0.0;
            con.gridwidth = 3;
            
            con.gridx = 0;
            con.gridy = 0;
            StudentPanel.add(timeLeft);
            con.gridx = 0;
            con.gridy = 1;
            StudentPanel.add(new JLabel(" " + Constants.NAME + ": "), con);
            con.gridx = 0;
            con.gridy = 2;
            StudentPanel.add(newName, con);
            con.gridx = 0;
            con.gridy = 3;
            StudentPanel.add(new JLabel(" " + Constants.GROUP + ": "), con);
            con.gridx = 0;
            con.gridy = 4;
            StudentPanel.add(newGroup, con);
            con.gridx = 0;
            con.gridy = 5;
            StudentPanel.add(new JLabel(" " + Constants.JAVA_PROF + ": "), con);
            con.gridx = 0;
            con.gridy = 6;
            StudentPanel.add(newProficiency, con);
            con.gridx = 0;
            con.gridy = 7;
            StudentPanel.add(new JLabel(" " + Constants.CPR + ": "), con);
            con.gridx = 0;
            con.gridy = 8;
            StudentPanel.add(newCpr, con);
            con.gridx = 0;
            con.gridy = 9;
            StudentPanel.add(new JLabel(" " + Constants.GENDER + ": "), con);
            con.gridx = 0;
            con.gridy = 10;
            StudentPanel.add(newGender, con);
            con.gridx = 0;
            con.gridy = 11;
            StudentPanel.add(new JLabel(" " + Constants.ADDSTUD_DL_LBL + ": "), con);
            con.gridx = 0;
            con.gridy = 12;
            StudentPanel.add(degreeLevels, con);
            con.gridx = 0;
            con.gridy = 13;
            StudentPanel.add(new JLabel(" " + Constants.ADDSTUD_TA_LBL + ": "), con);
            con.gridx = 0;
            con.gridy = 14;
            con.gridheight = 50;
            con.weightx = 50;
            StudentPanel.add(scrollCourses, con);
            int record = JOptionPane.showConfirmDialog(null, StudentPanel, Constants.ADD, JOptionPane.OK_CANCEL_OPTION);
            if(record == JOptionPane.OK_OPTION)
            {
                // Ensure the data types is what it should be
                String n       = newName.getText();
                String g       = newGroup.getText();
                double p       = Double.parseDouble(newProficiency.getText());
                String c       = (String)newCpr.getText();
                String ge      = String.valueOf(newGender.getSelectedItem());
                String lvl     = String.valueOf(degreeLevels.getSelectedItem().toString());
                List support   = courses.getSelectedValuesList();
                
                switch(lvl)
                {
                    case Constants.DEGREE_PHD:
                        PhdStudent student = new PhdStudent(n, p, c, ge);
                        student.addCourseSupport(support);
                        student.setDegreeLevel(lvl);
                        student.setGroup(g);
                        student.saveToFile();
                        
                        Student fs = new Student();
                        
                        Cli.m("" + fs.readFromFile().averageNote());
                        
                        // Output to dialog using overriden toString() method
                        JOptionPane.showMessageDialog(null, student.toString());
                        break;
                    case Constants.DEGREE_MSC:
                        if(support.size() > 0)
                        {
                            TaStudent ta = new TaStudent(n, p, c, ge);
                            ta.addCourseSupport(support);
                            ta.setDegreeLevel(lvl);
                            ta.setGroup(g);
                            ta.saveToFile();
                            // Output to dialog using overriden toString() method
                            JOptionPane.showMessageDialog(null, ta.toString());
                            break;
                        }
                    default:
                        Student stu = new Student(n, p, c, ge);
                        stu.setDegreeLevel(lvl);
                        stu.setGroup(g);
                        stu.saveToFile();
                        // Output to dialog using overriden toString() method
                        JOptionPane.showMessageDialog(null, stu.toString());
                        break;
                }
            } else {
                JOptionPane.showMessageDialog(null, Constants.NO_ACTION);
                System.exit(0);
            }
        }
    }
    
    /**
     * Performs CLI commands for ST, PHD, TA
     * 
     * @param welcome
     * @throws SecurityException
     * @throws NoSuchMethodException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InvocationTargetException 
     */
    public void performCliCommands(boolean welcome) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        // Init an empty command string
        String command;
        // Init input window
        InputStreamReader input = new InputStreamReader(System.in);
        // Store the types stuff in memory
        BufferedReader reader = new BufferedReader(input);
        // Create a new Cli instance
        Cli cli = new Cli(welcome);
        // Exit command message
        System.out.println(Constants.EXIT_HELP_MESSAGE);
        try
        {
            // Get the stuff typed in the cli
            command = reader.readLine().trim().toUpperCase();
            // If the command was not exit)
            if(!"EXIT".equals(command))
            {
                // If the user did actually typed something
                if(command.length() > 0)
                {
                    // Split the typed stuff into piece from 'space'
                    String cmds[] = command.split("\\s+");
                    // If the command typed is not one that requires
                    // parameters
                    if(!cli.isCommandWIthParams(command))
                    {
                        // Treat every string as a single command
                        for(String cmd : cmds)
                        {
                            // Set the command memeber
                            cli.setCommand(cmd);
                            // Map the corresponding method
                            cli.mapCommandToMethod();
                            // Execute the command
                            cli.execute();
                        }
                    } else {
                        if(cmds.length > 1)
                        {
                            // Parse the command and its parameters
                            cli.parseParameterdCommand(command);
                            // Map corresponding method
                            cli.mapCommandToMethod();
                            // Execute the command
                            cli.execute();
                        } else {
                            cli.runHelp();
                        }
                    }
                } else {
                    cli.runHelp();
                }
                // Start from the beggining, without welcome message
                this.performCliCommands(false);
            } else {
                System.exit(0);
            }
            
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * 
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    public void a4e5() throws InstantiationException, IllegalAccessException
    {
        Student student = new Student();
        TaStudent ta    = new TaStudent();
        PhdStudent phd  = new PhdStudent();
        
        /*student = phd;
        student.display();
        
        student = (Student) phd;
        student.display();*/
        
        student = (Student) phd.getClass().getSuperclass().newInstance();
        student.display();
        
        //ta = (TaStudent) student;
        //ta.display();
        
        //ta = phd;
        //ta.display();
    }
    
    /**
     * Test for Assignment 6
     * @package Assignment 6
     * @throws QueueIsEmptyException 
     */
    public void queueClassTest() throws QueueIsEmptyException
    {
        Cli.m("Queue in Java");
        Cli.m("-----------------------");
        Cli.m("Adding items to the Queue");
        MyStringQueue queue = new MyStringQueue();
        queue.add("Java");
        queue.add(".NET");
        queue.add("Javascript");
        queue.add("HTML5");
        queue.add("Hadoop");
        Cli.m("Items in the queue..." + Constants.NEW_LINE + queue.toString());
        Cli.m("remove element: " + queue.remove());
        Cli.m("retrieve element: " + queue.element());
        Cli.m("remove and retrieve element, null if empty: " + queue.poll());
        Cli.m("retrieve element, null is empty " + queue.peek());

    }
    
    public void saveStudentsByGrade() throws IOException
    {
        double avg = Double.parseDouble(JOptionPane.showInputDialog(Constants.S_DESIRED_JVPRF));
        
        Student std = new Student();
        std.writeStudentsByAverageGrade(avg);
    }
    
    /**
     * Assignment 11
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void readAndWriteTest() throws FileNotFoundException, IOException
    {
        // Assignment 11, Task 3
        //Student s = new Student("John");
        //s.saveToFile();
        
        Student ss = new Student();
        Student inst = ss.readFromFile();
        Cli.m(inst.toString());
        
        // Assignment 11, Task 5
        char[] mask = {'a','e'};
        char c = '*';
        char[] toSet = {'o','u'};
        
        // Input from CLI
        System.out.print("Enter a sentence here: ");
        MyMaskForInputStream in = new MyMaskForInputStream(System.in,mask,c);
        in.print();
        
        in.clear();
        
        // Input from file
        System.out.print("Enter a sentence here2: ");
        FileInputStream testFile = new FileInputStream("test.txt");
        BufferedInputStream bis = new BufferedInputStream(testFile);
        MyMaskForInputStream fin = new MyMaskForInputStream(bis,mask,toSet);
        fin.print();
    }
}

/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.mmav.assigment4.exceptions
 * @class      QueueOverflowException
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.mmav.assignment9.exceptions;
/**
 * Exception for overflowing queue
 * @author Mihai-Marius Vlăsceanu
 */
public class QueueOverflowException extends Exception {
    /**
     * Parameterless constructor
     */
    public QueueOverflowException() {};
    /**
     * Constructor with message
     * @param message 
     */
    public QueueOverflowException(String message)
    {
        super(message);
    }
}

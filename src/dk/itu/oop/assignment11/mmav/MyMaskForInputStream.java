/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.assignment11.mmav
 * @class      MyMaskForInputStream
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.assignment11.mmav;

import dk.itu.oop.mmav.assignment10.Cli;
import dk.itu.oop.mmav.assignment10.MyLog;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @package Assignment 11, Task 5
 * @author Mihai-Marius Vlăsceanu
 */
public class MyMaskForInputStream {
    
    // Store the input
    private InputStream in = null;
    // Hold the characters tobe replaced
    private ArrayList<String> target = new ArrayList<>();
    // Hold the replacements
    private ArrayList<String> replacement = new ArrayList<>();
    
    /**
     * Constructor for char[] => char
     * @param in
     * @param mask
     * @param c 
     */
    public MyMaskForInputStream(InputStream in, char[] mask, char c) 
    {
        this.setIn(in);
        this.setTarget(mask);
        this.setReplacement(c);
    }
    
    /**
     * Constructor for char[] => char[]
     * @param in
     * @param fromSet
     * @param toSet 
     */
    public MyMaskForInputStream(InputStream in, char[] fromSet, char[] toSet)
    {
        this.setIn(in);
        this.setTarget(fromSet);
        this.setReplacements(toSet);
    }
    
    /**
     * InputStream setter
     * @param in 
     */
    public final void setIn(InputStream in)
    {
        this.in = in;
    }
    
    /**
     * InputStream getter
     * @return 
     */
    public InputStream getIn()
    {
        return this.in;
    }
    
    /**
     * Target characters setter
     * @param t 
     */
    public final void setTarget(char[] t)
    {
        for(char ch : t)
        {
            String v = String.valueOf(ch);
            this.getTarget().add(v);
        }
    }
    
    /**
     * Target characters getter
     * @return 
     */
    public ArrayList<String> getTarget()
    {
        return this.target;
    }
    
    /**
     * Replacements setter for char[]
     * @param r 
     */
    public final void setReplacements(char[] r)
    {
        for(char ch : r)
        {
            String v = String.valueOf(ch);
            this.getReplacement().add(v);
        }
    }
    
    /**
     * Replacement setter for char
     * @param r 
     */
    public final void setReplacement(char r)
    {
        String v = String.valueOf(r);
        this.getReplacement().add(v);
    }
    
    /**
     * Getter for replacement
     * @return 
     */
    public ArrayList<String> getReplacement()
    {
        return this.replacement;
    }
    
    /**
     * Prints out the text with replaced characters
     * @throws IOException 
     */
    public void print() throws IOException
    {
        Cli.m(this.replace().toString());
    }
    
    /**
     * Glues together an array of strings
     * @param separator
     * @param data
     * @return 
     */
    public String implode(String separator, ArrayList<String> data) 
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.size() - 1; i++) {
            if (!data.get(i).matches(" *")) {
                sb.append(data.get(i));
                sb.append(separator);
            }
        }
        sb.append(data.get(data.size() - 1));
        return sb.toString();
    }
    
    /**
     * Maps paired replacements
     * @return 
     * @throws java.lang.Exception 
     */
    public Map<String, String> mapReplacements() throws Exception
    {
        Map<String, String> map = new HashMap();
        
        int i = 0;
        for(String ch : this.target)
        {
            if(this.getReplacement().size() == 1)
            {
                map.put(ch, this.getReplacement().get(0));
            }
            else if(this.getReplacement().size() == this.getTarget().size())
            {
                map.put(ch, this.getReplacement().get(i));
            } else {
                throw new Exception("Replacements sizes are incorrect!");
            }
            i++;
        }
        return map;
    }
    
    /**
     * Performs the character replacements in accordance
     * with the Map
     * @return
     * @throws IOException 
     */
    public StringBuffer replace() throws IOException
    {
        // Implode target characters
        String chars = implode("|", this.getTarget());
        
        // Create the pattern with the chars
        Pattern p = Pattern.compile(chars);
        
        // Read the input
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(this.in));
        String s = bufferRead.readLine();
        
        // Get the map
        Map<String, String> map;
        try {
            map = this.mapReplacements();
        
        
        // Find the characters
        Matcher m = p.matcher(s);
        
        // Replace the characters and store the text
        // in the memory
        StringBuffer sb = new StringBuffer();
        while (m.find()){
            m.appendReplacement(sb, map.get(m.group()));
        }
        m.appendTail(sb);
        
            return sb;
        } catch (Exception ex) {
            MyLog.log(ex.getMessage(), MyLog.TO_CLI);
        }
        return null;
    }
    
    /**
     * Clears the stored data
     */
    public void clear()
    {
        this.in = null;
        this.getTarget().clear();
        this.getReplacement().clear();
    }
}

/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Object-Oriented Programming Projects
 * @package    dk.itu.oop.assignment11.mmav
 * @class      FileInfo
 * @copyright  Copyright (c) 2014 Mihai-Marius Vlăsceanu (mmav@itu.dk)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu
 */

package dk.itu.oop.assignment11.mmav;

import dk.itu.oop.mmav.assignment10.Cli;
import dk.itu.oop.mmav.assignment10.Constants;
import dk.itu.oop.mmav.assignment10.MyLog;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @package Assignment 11, Task 2
 * @author Mihai-Marius Vlăsceanu
 */
public class FileInfo {
    // Store the path
    private File path;
    // Counter for files
    private static int counter = 0;
    // Init an output var
    private StringBuilder output = new StringBuilder();
    
    /**
     * Constructor which initiates the path
     * @param path 
     */
    public FileInfo(String path)
    {
        this.init(path);
    }
    
    /**
     * Initiates the path and it creates directories
     * f they don't exist
     * @param path 
     */
    public final void init(String path)
    {
        this.setPath(new File(path));
        this.getPath().mkdirs();
    }
    
    /**
     * Path setter
     * @param f 
     */
    public final void setPath(File f)
    {
        this.path = f;
    }
    
    /**
     * Path getter
     * @return 
     */
    public final File getPath()
    {
        return this.path;
    }
    
    /**
     * Counts the files in a directory (non-recursive)
     * @return 
     */
    public int countFiles()
    {
        for(File f : this.getPath().listFiles())
        {
            if(f.isFile())
                FileInfo.counter++;
        }
        return FileInfo.counter;
    }
    
    /**
     * Counts all the files in a path and subpaths
     * @param path
     * @return 
     */
    public int countFilesRecursively(File path)
    {        
        for(File f : path.listFiles())
        {
            if(f.isFile()) {
                FileInfo.counter++;
            }
            if(f.isDirectory()) {
                this.countFilesRecursively(f);
                break;
            }       
        }
        return FileInfo.counter;
    }
    
    /**
     * Non-recursive file listing
     * @return
     * @throws IOException 
     */
    public String listFiles() throws IOException
    {
        for(File f : this.getPath().listFiles())
        {
            if(f.isFile()) {
                // Counter at the beginning
                output.append(FileInfo.counter);
                output.append(": ");
                
                // File name
                output.append(f.getName());
                output.append(" | ");
                
                // Size
                String size = FileInfo.formatFileSize((int) f.length());
                output.append(size);
                output.append(" | ");
                
                // Mime type
                output.append(Files.probeContentType(f.toPath()));
                output.append(" | ");
                
                // Extension
                String extension = f.getName().replaceAll("^.*\\.([^.]+)$", "$1");
                output.append(extension);
                output.append(Constants.NEW_LINE);
                FileInfo.counter++;
            }
        }
        return output.toString();
    }
    
    /**
     * Recursive file listing with extension filter
     * @param path
     * @param filter
     * @return
     * @throws IOException 
     */
    public String listFilesRecursively(File path, String filter) throws IOException
    {
        for(File f : path.listFiles())
        {
            // Get the file extension (without .)
            String extension = f.getName().replaceAll("^.*\\.([^.]+)$", "$1");
            
            if(f.isFile() && (filter == null || "".equals(filter) || (filter == null ? extension == null : filter.equals(extension)))) {
                // Counter at the beginning
                output.append(FileInfo.counter);
                output.append(": ");
                
                // FIle name
                output.append(f.getName());
                output.append(" | ");
                
                // File Size
                String size = FileInfo.formatFileSize((int) f.length());
                output.append(size);
                output.append(" | ");
                
                // Mime Type
                output.append(Files.probeContentType(f.toPath()));
                output.append(" | ");
                
                // Extension
                output.append(extension);
                output.append(Constants.NEW_LINE);
                
                FileInfo.counter++;
            }
            
            if(f.isDirectory())
            {
                return this.listFilesRecursively(f, filter);
            }
        }
        return output.toString();
    }
    
    /**
     * COunter back to 0
     */
    public static void resetCounter()
    {
        FileInfo.counter = 0;
    }
    
    /**
     * Clear the output string
     */
    public void resetOutput()
    {
        this.output = new StringBuilder();
    }
    
    /**
     * Formats the file sizes
     * @param s
     * @return 
     */
    public static String formatFileSize(int s)
    {
        // Bytes
        if(s < 1024)
            return s + "B";
        // KiloBytes
        else if(s < (1024*1024))
            return ((s/1024) + "KB");
        // MegaBytes
        else if(s < (1024 * 1024 * 1024))
            return (s/1024/1024 + "MB");
        // GigaBytes
        else if(s < (1024 * 1024 * 1024 * 1024))
            return (s/1024/1024/1024 + "GB");
        return "" + s;
    }
    
    /**
     * Main runner
     * @param args 
     */
    public static void main(String[] args)
    {
        String path = "oop";
        FileInfo f = new FileInfo(path);
        
        Cli.m("Recursive count: ");
        Cli.m("" + f.countFilesRecursively(f.getPath()));
        FileInfo.resetCounter();
        f.resetOutput();
        
        try {
            Cli.m("File list: ");
            Cli.m(f.listFiles());
            FileInfo.resetCounter();
            f.resetOutput();
            
        } catch (IOException ex) {
            MyLog.log(ex.getMessage(), MyLog.TO_CLI);
        }
        
        try {
            Cli.m("Recursive file list: ");
            Cli.m(f.listFilesRecursively(f.getPath(), null));
            FileInfo.resetCounter();
            f.resetOutput();
            
        } catch (IOException ex) {
            MyLog.log(ex.getMessage(), MyLog.TO_CLI);
        }
        
        try {
            Cli.m("Recursive file list with filter: ");
            Cli.m(f.listFilesRecursively(f.getPath(), "pdf"));
            FileInfo.resetCounter();
            f.resetOutput();
            
        } catch (IOException ex) {
            MyLog.log(ex.getMessage(), MyLog.TO_CLI);
        }
    }
}
    